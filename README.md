# README #

This project contains the solutions to the questions asked and the necessary steps to test the application.  
All the questions have been addressed using **C# language**. Each of the solution resides in its directory within the project

### **Prerequisites** ###

* IDE ([Visual Studio](https://visualstudio.microsoft.com/downloads/) (preferred) or [VS Code](https://code.visualstudio.com/))
* .NET Core 3.1 SDK. Download [HERE](https://dotnet.microsoft.com/download/dotnet-core/3.1)

### **Quick SetUp Option [Preferred]**

Once you've installed visual studio, you can automatically open the Solution file **Quiz.sln** using visual studio, which will in turn download the required dependencies.

To run any project, right click on the project e.g **QuestionOne**, then select **set as startup project**. From there you can click the run button and the project will start running. See the run button [HERE](https://drive.google.com/file/d/19ocZGGnlVUgyqMsWjBBp_QG6yMlPqml6/view?usp=sharing)

Each of the question has been done as a separate Project but within the same solution. You can repeat the step above for the other questions.

### **Question One** ###

* We can calculate the distance between the point P and the circle's center C by using the distance formula, If the point P is at (Px, Py) and the circle's center C is at (Cx, Cy), then the following formula gives the distance (D) between the two points.

Let us define some variables
```sh
$ V1 = (Cx-Px)^2
$ V2 = (Cy-Py)^2
```
With the variables above, then we can get the distance by
```sh
$ Distance(D) = sqrt(V1+V2)
```


Logic to get the points earned is as shown below

```sh
        public int GetAmountEarned(int px, int py)
        {           
            // **lets define the board ...**
            const int outerCircleRadius = 10;
            const int middleCircleRadius = 5;
            const int innerCircleRadius = 1;


            // now check the distance falls under which circle within the target and return the value ...

            if (D > outerCircleRadius)
            {
                return 0;
            }

            if (D > middleCircleRadius)
            {
                return 1;
            }

            return D > innerCircleRadius ? 5 : 10;
        }
```



This project also contains unit tests which test various scenarios for the dart game.

### Question One Build | Run Steps ###

From within the root project directory execute the following commands
```sh
$ cd Quiz
$ cd QuestionOne
$ dotnet build
$ dotnet run
```
The source code/solution for this question is [HERE](https://bitbucket.org/Eric3770/im-bank-quiz/src/master/Quiz/QuestionOne/)


### **Question Two (Knapsack problem)** ###

Given a set of items, each with a weight and a value, determine the number of each item to include in a collection so that the total weight is less than or equal to a given limit and the total value is as large as possible. This solves the problem faced by someone who is constrained by a fixed-size (knapsack) and must fill it with the most valuable items. The problem often arises in resource allocation where the decision makers have to choose from a set of non-divisible projects or tasks under a fixed budget or time constraint, respectively.

This project also contains unit tests which test various scenarios for the knapsack problem.

### Question Two Build | Run Steps ###

From within the root project directory execute the following commands
```sh
$ cd Quiz
$ cd QuestionTwo
$ dotnet build
$ dotnet run
```
The source code/solution for this question is [HERE](https://bitbucket.org/Eric3770/im-bank-quiz/src/master/Quiz/QuestionTwo/)

### **Question Three (User Interface & SWAPI API Consumption)** ###

* PostgreSQL database has been used for persistence storage.
* Credential used for the DB can be found in [HERE](https://bitbucket.org/Eric3770/im-bank-quiz/src/master/Quiz/QuestionThree/appsettings.json) - Edit the **DefaultConnection** settings to suite your environment
* Import/Execute [THIS](https://bitbucket.org/Eric3770/im-bank-quiz/src/master/Quiz/im_bank.sql) script to create the database and the relevant tables.

To get up and running, execute the commands below from the terminal
```sh
$ cd Quiz
$ cd QuestionThree
$ dotnet build
$ dotnet run
```

The above build steps will expose the UI on port **5000**, localhost. To access the UI open the url below
```sh
$ http://localhost:5000
```


### **Question Four (IOU RESTFUL API)** ###

* PostgreSQL database has been used for persistence storage.
* Credential used for the DB can be found in [HERE](https://bitbucket.org/Eric3770/im-bank-quiz/src/master/Quiz/QuestionThree/appsettings.json) - Edit the **DefaultConnection** settings to suite your environment
* Import/Execute [THIS](https://bitbucket.org/Eric3770/im-bank-quiz/src/master/Quiz/im_bank.sql) script to create the database and the relevant tables.

To get up and running, execute the commands below from the terminal
```sh
$ cd Quiz
$ cd QuestionFour
$ dotnet build
$ dotnet run
```

The above build steps will expose the APIs on port **5010**, localhost. To access the APIs, send various request to the url below
```sh
$ http://localhost:5010 :- this is also the BaseUrl
```

The endpoints exposed are as shown below.

### GET [List User Information]: **/users**

Sample Response  

```sh
{
    "users": [
        "Adam",
        "Bob"
    ]
}
```


### POST [Create User]: **/add**

Sample Request  

```sh
{
    "user": {
        "name": "Adam",
        "owes": {
            "Bob": 12,
            "Chuck": 4,
            "Dan": 9.5
        },
        "owed_by": {
            "Bob": 6.5,
            "Dan": 2.75
        }
    }
}
```  

Sample Response

```sh
{
    "name": "Adam",
    "owes": {
        "Bob": 12,
        "Chuck": 4,
        "Dan": 9.5
    },
    "owed_by": {
        "Bob": 6.5,
        "Dan": 2.75
    },
    "balance": "-16.25"
}
```

### POST [Create IOU]: **/iou**

Sample Request  

```sh
{
    "lender": "Bob",
    "borrower": "Chuck",
    "amount": 30
}
```  

Sample Response

```sh
{
    "users": [
        "Adam",
        "Bob",
        "Chuck",
        "Dan"
    ]
}

```


### DELETE [Delete User]: **/users/{name}/remove**

Sample Request

```sh
http://localhost:5010/users/dan/remove
```  

Sample Response

```sh
{
    "users": [
        "Adam",
        "Bob",
        "Chuck"
    ]
}

```
You will find the unit test project [HERE](https://bitbucket.org/Eric3770/im-bank-quiz/src/master/Quiz/UnitTestProject/). It contains various test cases for Question 1 & Question 2  

### **The End** ###
 
  Thank you :smiley: