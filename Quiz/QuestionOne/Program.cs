﻿using System;

namespace QuestionOne
{

    // This will address the solution to the dart game question ...
    class Program
    {
        private static readonly int[] _pointA = { 0, 10 };
        private static readonly int[] _pointB = { 0, 5 };
        private static readonly int[] _pointC = { 0, 0 };
        static void Main(string[] args)
        {
            Console.WriteLine("\nHello, lets calculate the points earned for various points!\n\n");
            var dart = new Dart();

            var valueEarned = dart.GetAmountEarned(_pointA[0], _pointA[1]);
            Console.WriteLine($"Points earned for the point {{0,10}} is {valueEarned}\n");


            valueEarned = dart.GetAmountEarned(_pointB[0], _pointB[1]);
            Console.WriteLine($"Points earned for the point {{0,5}} is {valueEarned}\n");


            valueEarned = dart.GetAmountEarned(_pointC[0], _pointC[1]);
            Console.WriteLine($"Points earned for the point {{0,0}} is {valueEarned}\n");

            Console.ReadLine();
        }
    }


    public class Dart
    {
        // We can calculate the distance between the point P and the circle's center C by using the distance formula,
        // If the point P is at (Px, Py) and the circle's center C is at (Cx, Cy), then the following formula gives the distance (D) between the two points.

        /*
         * lets define some variables.
         * V1 = (Cx-Px)^2
         * V2 = (Cy-Py)^2
         *
         * Distance(D) = sqrt(V1+V2)
         */

        // in our case the center will always be at the coordinate (0,0)
        private int _cx = 0, _cy = 0;

        private double GetDistanceBetweenPoints(int px, int py)
        {

            var v1 = Math.Pow(_cx - px, 2); // getting (Cx-Px) exponentially by 2 ...
            var v2 = Math.Pow(_cy - py, 2); // getting (Cy-Py) exponentially by 2 ...

            var distance = Math.Sqrt(v1 + v2);

            return distance;
        }

        public int GetAmountEarned(int px, int py)
        {
            var distance = GetDistanceBetweenPoints(px, py);

            // lets define the board ...
            const int outerCircleRadius = 10;
            const int middleCircleRadius = 5;
            const int innerCircleRadius = 1;


            // now check the distance falls under which circle within the target and return the value ...

            if (distance > outerCircleRadius)
            {
                return 0;
            }

            if (distance > middleCircleRadius)
            {
                return 1;
            }

            return distance > innerCircleRadius ? 5 : 10;
        }
    }
}
