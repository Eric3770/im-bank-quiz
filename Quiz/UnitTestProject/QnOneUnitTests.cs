using Microsoft.VisualStudio.TestTools.UnitTesting;
using QuestionOne;

namespace UnitTestProject
{
    [TestClass]
    public class QnOneUnitTests
    {
        // These are sample points we'll use for testing ... they're in the format (X,Y). X = index 0, Y = index 1
        private readonly int[] _pointA = { 0, 10 };
        private readonly int[] _pointB = { 0, 5 };
        private readonly int[] _pointC = { 0, 0 };

        [TestMethod]
        public void TestAmountEarnedForPointA()
        {
            // Arrange ...
            var dart = new Dart();
            const int expectedValue = 1;

            // Act ...
            var actualValue = dart.GetAmountEarned(_pointA[0], _pointA[1]);

            // Assert ...
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void TestAmountEarnedForPointB()
        {
            // Arrange ...
            var dart = new Dart();
            const int expectedValue = 5;

            // Act ...
            var actualValue = dart.GetAmountEarned(_pointB[0], _pointB[1]);

            // Assert ...
            Assert.AreEqual(expectedValue, actualValue);
        }

        [TestMethod]
        public void TestAmountEarnedForPointC()
        {
            // Arrange ...
            var dart = new Dart();
            const int expectedValue = 10;

            // Act ...
            var actualValue = dart.GetAmountEarned(_pointC[0], _pointC[1]);

            // Assert ...
            Assert.AreEqual(expectedValue, actualValue);
        }
    }
}
