using Microsoft.VisualStudio.TestTools.UnitTesting;
using QuestionOne;
using QuestionTwo;

namespace UnitTestProject
{
    [TestClass]
    public class QnTwoUnitTests
    {

        private readonly Item[] itemsInTheBank = {
            new Item {Value = 60, Weight = 10},
            new Item {Value = 100, Weight = 20},
            new Item {Value = 120, Weight = 30},
        };

        private readonly Item[] itemsInTheBank_2 = {
            new Item {Value = 10, Weight = 5},
            new Item {Value = 40, Weight = 4},
            new Item {Value = 30, Weight = 6},
        };

        [TestMethod]
        public void TestMaximumLootViableWithCapacity50()
        {
            // Arrange ...
            var knapSack = new KnapSack();
            const int expectedValue = 220;

            // Act ...
            var actualValue = knapSack.GetMaximumValueViable(50, itemsInTheBank);

            // Assert ...
            Assert.AreEqual(expectedValue, actualValue);
        }


        [TestMethod]
        public void TestMaximumLootViableWithCapacity10()
        {
            // Arrange ...
            var knapSack = new KnapSack();
            const int expectedValue = 70;

            // Act ...
            var actualValue = knapSack.GetMaximumValueViable(10, itemsInTheBank_2);

            // Assert ...
            Assert.AreEqual(expectedValue, actualValue);
        }
    }
}
