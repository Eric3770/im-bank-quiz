﻿using System;
using System.Collections.Generic;
using System.Text;

namespace QuestionTwo
{
    public class Item
    {
        public int Weight { get; set; }
        public int Value { get; set; }
    }
}
