﻿using System;
using System.Text.Json.Serialization;

namespace QuestionTwo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, about to solve the knapsack problem!\n");

            var knapSack = new KnapSack();
            
            var lootValueViable = knapSack.GetMaximumValueViable(50, itemsInTheBank);
           
            
            Console.WriteLine($"Finding max loot for [{{value = 60, weight = 10}}, {{value = 100, weight = 20}}, {{value = 120, weight = 30}}]");
            Console.WriteLine($"Maximum value for a knapsack limit of 50 for the above items is: {lootValueViable}\n\n");


            lootValueViable = knapSack.GetMaximumValueViable(10, itemsInTheBank_2);
            Console.WriteLine($"Finding max loot for [{{value = 10,weight = 5}}, {{value = 40,weight = 4}}, {{value = 30,weight = 6}}]");
            Console.WriteLine($"Maximum value for a knapsack limit of 10 for the above items is: {lootValueViable}\n");

            Console.ReadLine();

        }

        private static readonly Item[] itemsInTheBank = {
            new Item {Value = 60, Weight = 10},
            new Item {Value = 100, Weight = 20},
            new Item {Value = 120, Weight = 30},
        };

        private static readonly Item[] itemsInTheBank_2 = {
            new Item {Value = 10, Weight = 5},
            new Item {Value = 40, Weight = 4},
            new Item {Value = 30, Weight = 6},
        };

    }

}
