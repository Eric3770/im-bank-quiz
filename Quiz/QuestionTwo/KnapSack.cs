﻿using System;
using System.Linq;

namespace QuestionTwo
{
    public class KnapSack
    {
        public int GetMaximumValueViable(int capacity, Item[] items)
        {
            // check if there are any items ...
            if (items.Length == 0 || capacity == 0)
                return 0;

            // check if there's only one item and if it fits the knapsack...
            if (items.Length == 1)
                return items[0].Weight < capacity ? items[0].Value : 0;

            // we'll keep track of the best loot seen :-)
            var best = 0;

            for (var i = 0; i < items.Length; i++)
            {
                // fetch the other items in the bank...
                var otherBankItems = items.Take(i).Concat(items.Skip(i + 1)).ToArray();

                // calculate the best value without the current item ...
                var without = GetMaximumValueViable(capacity, otherBankItems);
                var with = 0;


                // If the current item fits then calculate the best value for
                // a capacity less it's weight and with it removed from contention
                // and add the current items value to that.
                if (items[i].Weight <= capacity)
                {
                    with = GetMaximumValueViable(capacity - items[i].Weight, otherBankItems) + items[i].Value;
                }

                // The current best is the max of the with or without.
                var currentBest = Math.Max(without, with);

                // determine if the current best is the overall best.
                if (currentBest > best)
                    best = currentBest;
            }

            return best;
        }
    }
}
