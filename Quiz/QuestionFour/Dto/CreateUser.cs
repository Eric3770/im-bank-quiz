﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace QuestionFour.Dto
{
    public class CreateUser
    {
        [JsonProperty("name")]
        [Required]
        public string Name { get; set; }

        [JsonProperty("owes")]
        [Required(ErrorMessage = "The owes field is required")]
        public Dictionary<string, float> Owes { get; set; }

        [JsonProperty(PropertyName = "owed_by")]
        [Required(ErrorMessage = "The owed_by field is required")]
        [JsonPropertyName("owed_by")]
        public Dictionary<string, float> OwedBy { get; set; }

        [JsonProperty("balance")]
        public string Balance { get; set; }
    }


    public class CreateUserRequest
    {
        [JsonProperty("user")]
        [Required(ErrorMessage = "The user field is required")]
        public CreateUser User { get; set; }
    }
}
