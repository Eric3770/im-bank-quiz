﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace QuestionFour.Dto
{
    public class CreateIou
    {
        [Required]
        public string Lender { get; set; }

        [Required]
        public string Borrower { get; set; }

        [Required]
        public float Amount { get; set; }
    }
}
