﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuestionFour.DbContext;
using QuestionFour.Models;

namespace QuestionFour.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class BaseController : ControllerBase
    {
        protected readonly IOUDbContext _context;

        public BaseController(IOUDbContext context) => _context = context;

        protected async Task<User> GetUserByName(string name)
        {
            try
            {
                var user = await _context.Users.AsNoTracking().FirstOrDefaultAsync(w => w.Name.ToLower() == name.ToLower());
                return user;
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}