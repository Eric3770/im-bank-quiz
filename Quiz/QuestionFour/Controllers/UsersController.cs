﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using QuestionFour.DbContext;
using QuestionFour.Dto;
using QuestionFour.Models;

namespace QuestionFour.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UsersController : BaseController
    {
        public UsersController(IOUDbContext context)
            : base(context)
        { }

        // GET: /Users
        [HttpGet]
        public async Task<IActionResult> GetUsers()
        {
            try
            {
                var users = await _context.Users.OrderBy(o => o.Name).ToListAsync();

                var formattedUsers = new
                {
                    users = users.Select(s => s.Name).ToList()
                };

                return Ok(formattedUsers);
            }
            catch (Exception)
            {
                return StatusCode(500, new { Message = "Request could not be processed successfully. Please try again" });
            }

        }

        // GET: Users/5
        [HttpGet("{id}")]
        public async Task<ActionResult<User>> GetUser(int id)
        {
            var user = await _context.Users.FindAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            return user;
        }

        // PUT: Users/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutUser(int id, User user)
        {
            if (id != user.Id)
            {
                return BadRequest();
            }

            _context.Entry(user).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UserExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: /Users
        [HttpPost]
        [Route("/add")]
        public async Task<ActionResult<User>> PostUser(CreateUserRequest createUserRequest)
        {

            try
            {
                var users = new List<User>();
                var createUser = createUserRequest.User;

                // get the lenders ...
                float totalOwedByOtherUsers = 0, totalOwedToOtherUsers = 0;
                string balance;

                // get the lenders ...
                foreach (var keyValuePair in createUser.Owes)
                {
                    // add this to IOU table ...
                    await _context.Ious.AddAsync(new IOU
                    {
                        Lender = keyValuePair.Key,
                        Borrower = createUser.Name,
                        Amount = keyValuePair.Value
                    });

                    totalOwedToOtherUsers += keyValuePair.Value;
                    users.Add(new User
                    {
                        Name = keyValuePair.Key,
                        Balance = 0
                    });
                }

                // get the borrowers ...
                foreach (var keyValuePair in createUser.OwedBy)
                {
                    // add this to IOU table ...
                    await _context.Ious.AddAsync(new IOU
                    {
                        Lender = createUser.Name,
                        Borrower = keyValuePair.Key,
                        Amount = keyValuePair.Value
                    });

                    totalOwedByOtherUsers += keyValuePair.Value;
                    users.Add(new User
                    {
                        Name = keyValuePair.Key,
                        Balance = 0
                    });
                }

                // get the balance ... 
                balance = (totalOwedByOtherUsers - totalOwedToOtherUsers).ToString();

                var distinctUsers = users.Distinct(new UserEqualityComparer());

                foreach (var user in distinctUsers)
                {
                    if (await GetUserByName(user.Name) is null)
                        await _context.AddAsync(user);
                }

                var newUser = new User
                {
                    Balance = float.Parse(balance),
                    Name = createUser.Name
                };
                await _context.AddAsync(newUser);
                await _context.SaveChangesAsync();

                createUser.Balance = balance;

                return Ok(createUser);
            }
            catch (Exception)
            {
                return StatusCode(500, new { Message = "Request could not be processed successfully. Please try again" });
            }

        }

        // POST: /iou
        [HttpPost]
        [Route("/iou")]
        public async Task<IActionResult> PostIOU(CreateIou createIou)
        {
            try
            {
                // check if both the lender and the borrower exists ... if not create them...
                var users = new List<User>();

                var borrowerUser = await GetUserByName(createIou.Borrower);
                if (borrowerUser is null)
                {
                    users.Add(new User
                    {
                        Name = createIou.Borrower,
                        Balance = createIou.Amount * -1
                    });
                }
                else
                {
                    // update the balance for this user ...
                    borrowerUser.Balance += (createIou.Amount * -1);
                    _context.Update(borrowerUser);
                }

                var lenderUser = await GetUserByName(createIou.Lender);
                if (lenderUser is null)
                {
                    users.Add(new User
                    {
                        Name = createIou.Lender
                    });
                }
                else
                {
                    // update the balance for this user ...
                    lenderUser.Balance += createIou.Amount;
                    _context.Update(lenderUser);
                }

                // create the IOU ...
                var iou = new IOU
                {
                    Amount = createIou.Amount,
                    Borrower = createIou.Borrower,
                    Lender = createIou.Lender
                };

                await _context.AddAsync(iou);

                if (users.Count > 0)
                {
                    await _context.AddRangeAsync(users);
                }

                await _context.SaveChangesAsync();

                return await GetUsers();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { Message = "Your request could not be completed successfully. Please try again" });
            }
        }

        // DELETE: Users/eric
        [HttpDelete("{name}/remove")]
        public async Task<IActionResult> DeleteUser(string name)
        {
            try
            {
                var user = await GetUserByName(name);

                if (user is null)
                    return NotFound(new { Message = $"User with the name '{name}' does not exist." });

                // delete this user from Users table ...
                _context.Remove(user);

                // remove all the IOUs for the user ...
                var userName = user.Name.ToLower();
                var userIoUs = await _context.Ious
                    .Where(w => w.Borrower.ToLower() == userName || w.Lender.ToLower() == userName).ToListAsync();


                if (userIoUs != null && userIoUs.Count > 0)
                {
                    _context.RemoveRange(userIoUs);
                }

                await _context.SaveChangesAsync();
                return await GetUsers();
            }
            catch (Exception)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, new { Message = "Your request could not be completed successfully. Please try again" });
            }
        }

        // DELETE: Users/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<User>> DeleteUser(int id)
        {
            var user = await _context.Users.FindAsync(id);
            if (user == null)
            {
                return NotFound();
            }

            _context.Users.Remove(user);
            await _context.SaveChangesAsync();

            return user;
        }

        private bool UserExists(int id)
        {
            return _context.Users.Any(e => e.Id == id);
        }

    }
}
