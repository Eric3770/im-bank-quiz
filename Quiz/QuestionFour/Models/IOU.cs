﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestionFour.Models
{
    public class IOU
    {
        public int Id { get; set; }
        public string Lender { get; set; }
        public string Borrower { get; set; }
        public float Amount { get; set; }
    }
}
