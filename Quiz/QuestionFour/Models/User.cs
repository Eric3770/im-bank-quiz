﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestionFour.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public float Balance { get; set; }
    }


    public class UserEqualityComparer : IEqualityComparer<User>
    {
        public bool Equals(User x, User y)
            => x.Name.ToLower() == y.Name.ToLower();

        public int GetHashCode(User obj)
            => obj.Name.GetHashCode();
    }
}
