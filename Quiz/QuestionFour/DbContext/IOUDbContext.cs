﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using QuestionFour.Models;

namespace QuestionFour.DbContext
{
    public class IOUDbContext : Microsoft.EntityFrameworkCore.DbContext
    {
        public IOUDbContext(DbContextOptions<IOUDbContext> options)
            : base(options)
        { }

        public DbSet<User> Users { get; set; }
        public DbSet<IOU> Ious { get; set; }
    }
}
