﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using QuestionThree.DbContext;

namespace QuestionThree.Controllers
{
    public class BaseController : Controller
    {
        protected readonly HttpClient client;
        protected IMDbContext _dbContext;

        public BaseController(IHttpClientFactory clientFactory, IMDbContext dbContext)
        {
            _dbContext = dbContext;
            client = clientFactory.CreateClient("swapi");
        }


        protected HttpRequestMessage GetRequest(string resource)
        {
            var request = new HttpRequestMessage(HttpMethod.Get, resource);
            return request;
        }
    }
}