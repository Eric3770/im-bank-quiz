﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using QuestionThree.DbContext;
using QuestionThree.Dtos;
using QuestionThree.Models;

namespace QuestionThree.Controllers
{
    public class ActorsController : BaseController
    {
        private readonly ILogger<ActorsController> _logger;

        private People people;
        private Result result;

        public ActorsController(IHttpClientFactory clientFactory, ILogger<ActorsController> logger, IMDbContext dbContext)
            : base(clientFactory, dbContext)
        {
            _logger = logger;
        }

        public async Task<IActionResult> Index()
        {
            var persons = new List<PeopleExtended>();
            // fetch the swapi actors ...
            try
            {
                var request = GetRequest("people/");

                var response = await client.SendAsync(request);

                if (!response.IsSuccessStatusCode) return View(persons);

                var responseStream = await response.Content.ReadAsStringAsync();
                people = JsonConvert.DeserializeObject<People>(responseStream);

                foreach (var actorResult in people.Results)
                {
                    // get the id of the actor ..
                    var url = actorResult.Url.ToString();
                    var split = url.Split('/');
                    var actorId = split[split.Length - 2];
                    int.TryParse(actorId, out var intActorId);

                    // check if entry exists in the db ...
                    var dbEntry = await _dbContext.Actors.FirstOrDefaultAsync(f => f.PeopleId == intActorId);

                    var peopleExtended = new PeopleExtended
                    {
                        BirthYear = actorResult.BirthYear,
                        Created = actorResult.Created,
                        Edited = actorResult.Edited,
                        EyeColor = actorResult.EyeColor,
                        Films = actorResult.Films,
                        Gender = actorResult.Gender,
                        HairColor = actorResult.HairColor,
                        Height = actorResult.Height,
                        Homeworld = actorResult.Homeworld,
                        Name = actorResult.Name,
                        Mass = actorResult.Mass,
                        SkinColor = actorResult.SkinColor
                    };
                    if (dbEntry != null)
                        peopleExtended.IsFavorite = dbEntry.IsFavorite;

                    persons.Add(peopleExtended);
                }

                return View(persons);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error fetching actors: {e.Message}");
                return View(persons);
            }
        }


        private async Task<Result> GetSwapiActor(int id)
        {
            try
            {
                var request = GetRequest($"people/{id}/");

                var response = await client.SendAsync(request);

                if (!response.IsSuccessStatusCode) return null;

                var responseStream = await response.Content.ReadAsStringAsync();
                var actorsResult = JsonConvert.DeserializeObject<Result>(responseStream);
                return actorsResult;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<IActionResult> Details(int id)
        {
            try
            {
                var actor = await _dbContext.Actors.FirstOrDefaultAsync(w => w.PeopleId == id) ?? new Actor();
                actor.PeopleId = id;
                ViewData["result"] = await GetSwapiActor(id);

                return View(actor);
            }
            catch (Exception e)
            {
                _logger.LogError($"Error fetching actors: {e.Message}");
                return View();
            }

        }

        [HttpPost]
        public async Task<IActionResult> Details([Bind("Id,PeopleId,IsFavorite")]Actor actor)
        {
            if (!ModelState.IsValid)
            {
                ViewData["result"] = await GetSwapiActor(actor.PeopleId);
                return View(actor);
            }


            // check if the entry exists in the db ...
            // if exists, update, else insert ..
            var dbEntry = await _dbContext.Actors.AsNoTracking().FirstOrDefaultAsync(w => w.PeopleId == actor.PeopleId);

            // get the number of favorites selected ...
            var favoritesCount = await _dbContext.Actors.AsNoTracking().Where(w => w.IsFavorite).CountAsync();

            if (dbEntry is null)
            {
                if (favoritesCount > 4)
                {
                    // show a message that, all favorites selection have been exhausted..
                    ViewData["showError"] = true;
                    ViewData["result"] = await GetSwapiActor(actor.PeopleId);
                    return View(actor);
                }
                _dbContext.Add(actor);
            }
            else
            {
                _dbContext.Update(actor);
            }

            await _dbContext.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }


    }
}