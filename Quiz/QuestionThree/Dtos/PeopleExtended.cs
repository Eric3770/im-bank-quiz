﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestionThree.Dtos
{
    public class PeopleExtended : Result
    {
        public bool IsFavorite { get; set; }
    }
}
