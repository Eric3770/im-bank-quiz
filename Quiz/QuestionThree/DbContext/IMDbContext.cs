﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using QuestionThree.Models;

namespace QuestionThree.DbContext
{
    public class IMDbContext: Microsoft.EntityFrameworkCore.DbContext
    {
        public IMDbContext(DbContextOptions<IMDbContext> options)
            : base(options)
        { }

        public DbSet<Actor> Actors { get; set; }
    }
}
