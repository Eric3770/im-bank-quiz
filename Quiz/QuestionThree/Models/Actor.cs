﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace QuestionThree.Models
{
    public class Actor
    {
        public int Id { get; set; }
        public int PeopleId { get; set; }
        public bool IsFavorite { get; set; }
    }
}
